package main

import "fmt"
import "encoding/json"
import "net/http"
import "database/sql"
import _ "github.com/go-sql-driver/mysql"


type User struct {
	Name   string
	UserId int
}

type Youkai struct {
	Name string `json:"youkai_name"`
	Url string
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	y := getYoukai()

	// gen obj
	json, error := json.Marshal(y)

	if error != nil {
		return
	}

	fmt.Fprintf(w, string(json))
}

func getYoukai() []Youkai{
	db, _ := sql.Open("mysql", "root:@/GODB")

  rows, err := db.Query("SELECT * FROM db_sample")

	if err != nil {
		fmt.Println(err)
	}

	var youkaiArray []Youkai

	for rows.Next() {
		var id int
		var name string
		var url string
		if err := rows.Scan(&id, &name, &url); err != nil  {
			fmt.Println(err)
		}

		youkai := Youkai{name, url}
		youkaiArray = append(youkaiArray, youkai)
	}
	return youkaiArray
}
